﻿using filmonerileri.Models;
using filmönerileri.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace filmönerileri.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminController : Controller
    {

        ApplicationDbContext context = new ApplicationDbContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NewMovie()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewMovie(NewMoviewViewModel film, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if(image != null && image.ContentLength > 0)
                {
                    byte[] imageContent;
                    using(var reader = new BinaryReader(image.InputStream))
                    {
                        imageContent = reader.ReadBytes(image.ContentLength);
                    }

                    context.Films.Add(new Film
                    {
                        Category = context.Categories.Where(c=> c.Name == film.Category).FirstOrDefault(),
                        Rating = film.Rating,
                        Image = imageContent,
                        Description = film.Description,
                        Name = film.Name,
                        ReleaseYear = film.ReleaseYear                  
                    });
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(context != null)
            {
                context.Dispose();
            }
        }

    }
}